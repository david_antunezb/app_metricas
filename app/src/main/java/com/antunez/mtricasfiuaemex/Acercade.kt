package com.antunez.mtricasfiuaemex

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_acercade.*

class Acercade : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_acercade)

        //Acción Botón Regresar
        btn_Regresar.setOnClickListener {
            onBackPressed()
        }
    }
}
