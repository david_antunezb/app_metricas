package com.antunez.mtricasfiuaemex

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        Thread.sleep(1000)   //Se le da una espera para ser visible
        setTheme(R.style.AppTheme) //Se Manda a llamar el SplashScreen

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Acción del Botón Ingresar
        btnEntrar.setOnClickListener {
            val intent: Intent = Intent(this, Home::class.java)  //Definimos que Activity se va a llamar "Home"
            startActivity(intent)  //Se Hace visible
        }

        //Acción del Botón Acerca De
        btn_Acercade.setOnClickListener {
            val intent: Intent = Intent(this, Acercade::class.java)
            startActivity(intent)
        }
    }
}
