package com.antunez.mtricasfiuaemex

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_metricas.*
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader


class Metricas : AppCompatActivity() {
    //DEFINICIÓN DE ATRIBUTOS
    private val READ_REQUEST_CODE: Int = 42
    private var nom_Arch = ""

    private var n1 = 9.0;  //Numero de Operadores
    private var N1 = 0;  //Total de Operadores
    private var n2 = 6.0;  //Numero de Operandos
    private var N2 = 0;  //Total de Operandos
    private var N = 0;   //Longitud del Programa
    private var n = 0.0;   //Vocabulario
    private var V = 0.0;   //Volumen
    private var D = 0.0;   //Dificultad
    private var L = 0.0;   //Nivel del Programa
    private var E = 0.0;   //Esfuerzo
    private var T = 0.0;   //Tiempo (seg)
    private var B = 0.0;   //Número de Bugs


    //- - - - - - - - - - OPERADORES - - - - - - - - - -
    private val retorna = "return"
    private val tipo1 = "int"
    private var moda_tipo1 = 0

    private val tipo2 = "float"
    private var moda_tipo2 = 0

    private val tipo3 = "char"
    private var moda_tipo3 = 0

    private val parentesis = "("
    private var moda_parentesis = 0

    private val llave = "{"
    private var moda_llave = 0

    private val corchete = "["
    private var moda_corchete = 0

    private val puntoComa = ";"
    private var moda_puntoComa = 0


    //DE ASIGNACIÓN
    private val asignacion = "="
    private var moda_asignacion = 0

    //INCREMENTALES / DECRE
    private val aumenta = "++"
    private var moda_aumenta = 0

    private val disminuye = "--"
    private var moda_disminuye = 0

    //ARITMÉTICOS
    private val suma = "+"
    private var moda_suma = 0

    private val resta = "-"
    private var moda_resta = 0

    private val multiplicacion = "*"
    private var moda_multiplicacion = 0

    private val division = "/"
    private var moda_division = 0

    private val modulo = "%"
    private var moda_modulo = 0

    //OPERACIONALES
    private val mayor = ">"
    private var moda_mayor = 0

    private val mayorIgual = ">="
    private var moda_mayorIgual = 0

    private val menor = "<"
    private var moda_menor = 0

    private val menorIgual = "<="
    private var moda_menorIgual = 0

    private val igual = "=="
    private var moda_igual = 0

    private val diferente = "!="
    private var moda_diferente = 0

    //LÓGICOS
    private val and = "&&"
    private var moda_and = 0

    private val or = "||"
    private var moda_or = 0

    private val negacion = "!"
    private var moda_negacion = 0

    //ESTRUCTURAS DE CONTROL
    private val mientras = "while"
    private var moda_mientras = 0

    private val repite = "do"
    private var moda_repite = 0

    private val si = "if"
    private var moda_si = 0


    private val para = "for"
    private var moda_para = 0

    //PANTALLA
    private val escribe = "printf"
    private var moda_escribe = 0

    private val lee = "scanf"
    private var moda_lee = 0



    // - - - - - - - - - - OPERANDOS - - - - - - - - - -
    private val nombrefun = "main"
    private var moda_nombrefun = 0

    //POSIBLES NÚMEROS
    private val mil = "1000"
    private var moda_mil = 0

    private val ventiuno = "21"
    private var moda_ventiuno = 0

    private val cero = "0"
    private var moda_cero = 0

    private val uno = "1"
    private var moda_uno = 0

    private val dos = "2"
    private var moda_dos = 0

    //POSIBLES LETRAS (VARIABLES)
    private val a = "a"
    private var moda_a = 0

    private val b = "b"
    private var moda_b = 0

    private val c = "c"
    private var moda_c = 0

    private val l = "l"
    private var moda_l = 0

    private val x = "x"
    private var moda_x = 0

    private val y = "y"
    private var moda_y = 0

    private val z = "z"
    private var moda_z = 0

    private val palabra = "palabra"
    private var moda_palabra = 0

    private val perim = "perim"
    private var moda_perim = 0

    private val radio = "radio"
    private var moda_radio = 0

    private val pal_suma = "suma"
    private var moda_pal_suma = 0

    private val num = "num"
    private var moda_num = 0

    private val res = "res"
    private var moda_res = 0

    private val multiplicador = "multiplicador"
    private var moda_multiplicador = 0

    private val multiplicando = "multiplicando"
    private var moda_multiplicando = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_metricas)

        //Ocultamos La leyenda, el Campo de Código y el Botón
        titulo_codigo_fuente.visibility = View.INVISIBLE
        txt_Contenido.visibility = View.INVISIBLE
        btn_Calcular.visibility = View.INVISIBLE

        //Acción Botón Regresar
        btn_Regresar.setOnClickListener {
            onBackPressed()
        }

        //Acción Botón Buscar
        btn_SelectArch.setOnClickListener {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = "*/*"
            }
            startActivityForResult(intent, READ_REQUEST_CODE)
        }

        //Acción Botón Calcular Métricas
        btn_Calcular.setOnClickListener {
            //CALCULAMOS TODAS LAS MÉTRICAS
            calculaMetricas()

            //Llamamos a la Vista de Resultados y Enviamos los parámetros
            val intent:Intent = Intent(this, Resultados::class.java)

            intent.putExtra("nom_Arch", nom_Arch)
            intent.putExtra("n1", n1)
            intent.putExtra("N1", N1)
            intent.putExtra("n2", n2)
            intent.putExtra("N2", N2)
            intent.putExtra("N", N)
            intent.putExtra("n", n)
            intent.putExtra("V", V)
            intent.putExtra("D", D)
            intent.putExtra("L", L)
            intent.putExtra("E", E)
            intent.putExtra("T", T)
            intent.putExtra("B", B)

            startActivity(intent)



        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        super.onActivityResult(requestCode, resultCode, resultData)
        if (requestCode == READ_REQUEST_CODE && resultCode == AppCompatActivity.RESULT_OK) {

            //Mostramos La leyenda, el Campo de Código y el Botón
            titulo_codigo_fuente.visibility = View.VISIBLE
            txt_Contenido.visibility = View.VISIBLE
            btn_Calcular.visibility = View.VISIBLE

            resultData?.data?.also { uri ->
                var resultado:String=resultData.dataString.toString()  //Contiene toda la URL
                resultado=resultado.substring(resultado.lastIndexOf("F")+1)

                nom_Arch=resultado.substring(resultado.lastIndexOf("F")+1)  //Contiene el NOmbre del Archivo

                val result = readTextFromUri(uri);

                titulo_codigo_fuente.text = nom_Arch
                txt_Contenido.setText(result)
            }
        }
    }

    @Throws(IOException::class)
    private fun readTextFromUri(uri: Uri): String {
        val stringBuilder = StringBuilder()
        contentResolver.openInputStream(uri)?.use { inputStream ->
            BufferedReader(InputStreamReader(inputStream)).use { reader ->
                var line: String? = reader.readLine()
                while (line != null) {
                    stringBuilder.append(line + "\n")

                    //Llamamos a la función para Analizar
                    analizaCodigo(line)

                    line = reader.readLine()
                }
            }
        }
        return stringBuilder.toString()
    }

    //Función para mostrar Ventana de Alert
    private fun showAlert(message: String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Archivo Seleccionado")
        builder.setMessage(message)

        val dialog = builder.create()
        dialog.show()
    }

    private fun calculaMetricas(){
        N = N1 + N2             //Numero de Tokens

        n = n1 + n2             //Vocabulario de P

        V = N * Math.log(n)     //Volumen

        D = (n1/2)*(N2/n2)      //Dificultad

        L = 1/D                 //Nivel

        E = D*V                 //Esfuerzo

        T = E/18                //Tiempo en Seg

        B = Math.pow(E,((2/3).toDouble()))/3000    //Bugs

    }

    private fun analizaCodigo(line: String){
        /*PROPÓSITO: Incrementará los contadores de las Métricas*/

        if ( line.indexOf(nombrefun) != -1 ){
            if ( moda_nombrefun  == 0 ){  //INCREMENTA CONTADOR
                N2++
                n2++
                moda_nombrefun++
            }else{
                N2++
            }
        }

        if ( line.indexOf(tipo1) != -1 ){
            if ( moda_tipo1  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_tipo1++
            }else{
                N1++
            }
        }

        if ( line.indexOf(parentesis) != -1 ){
            if ( moda_parentesis  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_parentesis++
            }else{
                N1++
            }
        }

        if ( line.indexOf(llave) != -1 ){
            if ( moda_llave  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_llave++
            }else{
                N1++
            }
        }

        if ( line.indexOf(tipo2) != -1 ){
            if ( moda_tipo2  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_tipo2++
            }else{
                N1++
            }
        }

        if ( line.indexOf(tipo3) != -1 ){
            if ( moda_tipo3  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_tipo3++
            }else{
                N1++
            }

        }

        if ( line.indexOf(puntoComa) != -1 ){
            if ( moda_puntoComa  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_puntoComa++
            }else{
                N1++
            }
        }

        if ( line.indexOf(a) != -1 ){
            if ( moda_a  == 0 ){  //INCREMENTA CONTADOR
                N2++
                n2++
                moda_a++
            }else{
                N2++
            }

        }

        if ( line.indexOf(b) != -1 ){
            if ( moda_b  == 0 ){  //INCREMENTA CONTADOR
                N2++
                n2++
                moda_b++
            }else{
                N2++
            }
        }

        if ( line.indexOf(c) != -1 ){
            if ( moda_c  == 0 ){  //INCREMENTA CONTADOR
                N2++
                n2++
                moda_c++
            }else{
                N2++
            }

        }

        if ( line.indexOf(l) != -1 ){
            if ( moda_l  == 0 ){  //INCREMENTA CONTADOR
                N2++
                n2++
                moda_l++
            }else{
                N2++
            }
        }

        if ( line.indexOf(x) != -1 ){
            if ( moda_x  == 0 ){  //INCREMENTA CONTADOR
                N2++;
                n2++
                moda_x++
            }else{
                N2++
            }
        }

        if ( line.indexOf(y) != -1 ){
            if ( moda_y  == 0 ){  //INCREMENTA CONTADOR
                N2++
                n2++
                moda_y++
            }else{
                N2++
            }
        }

        if ( line.indexOf(z) != -1 ){
            if ( moda_z  == 0 ){  //INCREMENTA CONTADOR
                N2++
                n2++
                moda_z++
            }else{
                N2++
            }
        }

        if ( line.indexOf(palabra) != -1 ){
            if ( moda_palabra  == 0 ){  //INCREMENTA CONTADOR
                N2++
                n2++
                moda_palabra++
            }else{
                N2++
            }
        }

        if ( line.indexOf(perim) != -1 ){
            if ( moda_perim  == 0 ){  //INCREMENTA CONTADOR
                N2++
                n2++
                moda_perim++
            }else{
                N2++
            }
        }

        if ( line.indexOf(radio) != -1 ){
            if ( moda_radio  == 0 ){  //INCREMENTA CONTADOR
                N2++
                n2++
                moda_radio++
            }else{
                N2++
            }
        }

        if ( line.indexOf(pal_suma) != -1 ){
            if ( moda_pal_suma  == 0 ){  //INCREMENTA CONTADOR
                N2++
                n2++
                moda_pal_suma++
            }else{
                N2++
            }
        }

        if ( line.indexOf(num) != -1 ){
            if ( moda_num  == 0 ){  //INCREMENTA CONTADOR
                N2++
                n2++
                moda_num++
            }else{
                N2++
            }
        }

        if ( line.indexOf(res) != -1 ){
            if ( moda_res  == 0 ){  //INCREMENTA CONTADOR
                N2++
                n2++
                moda_res++
            }else{
                N2++
            }
        }
        if ( line.indexOf(multiplicador) != -1 ){
            if ( moda_multiplicador  == 0 ){  //INCREMENTA CONTADOR
                N2++
                n2++
                moda_multiplicador++
            }else{
                N2++
            }
        }
        if ( line.indexOf(multiplicando) != -1 ){
            if ( moda_multiplicando  == 0 ){  //INCREMENTA CONTADOR
                N2++
                n2++
                moda_multiplicando++
            }else{
                N2++
            }
        }

        if ( line.indexOf(escribe) != -1 ){
            if ( moda_escribe  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_escribe++
            }else{
                N1++
            }
        }

        if ( line.indexOf(lee) != -1 ){
            if ( moda_lee  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_lee++
            }else{
                N1++
            }
        }

        if ( line.indexOf(asignacion) != -1 ){
            if ( moda_asignacion  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_asignacion++
            }else{
                N1++
            }
        }

        if ( line.indexOf(corchete) != -1 ){
            if ( moda_corchete  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_corchete++
            }else{
                N1++
            }
        }

        if ( line.indexOf(aumenta) != -1 ){
            if ( moda_aumenta  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_aumenta++
            }else{
                N1++
            }
        }

        if ( line.indexOf(disminuye) != -1 ){
            if ( moda_disminuye  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_disminuye++
            }else{
                N1++
            }
        }

        if ( line.indexOf(suma) != -1 ){
            if ( moda_suma  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_suma++
            }else{
                N1++
            }
        }

        if ( line.indexOf(resta) != -1 ){
            if ( moda_resta  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_resta++
            }else{
                N1++
            }
        }

        if ( line.indexOf(multiplicacion) != -1 ){
            if ( moda_multiplicacion  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_multiplicacion++
            }else{
                N1++
            }
        }

        if ( line.indexOf(division) != -1 ){
            if ( moda_division  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_division++
            }else{
                N1++
            }
        }

        if ( line.indexOf(modulo) != -1 ){
            if ( moda_modulo  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_modulo++
            }else{
                N1++
            }
        }

        if ( line.indexOf(mayor) != -1 ){
            if ( moda_mayor  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_mayor++
            }else{
                N1++
            }
        }

        if ( line.indexOf(mayorIgual) != -1 ){
            if ( moda_mayorIgual  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_mayorIgual++
            }else{
                N1++
            }
        }

        if ( line.indexOf(menor) != -1 ){
            if ( moda_menor  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_menor++
            }else{
                N1++
            }
        }

        if ( line.indexOf(menorIgual) != -1 ){
            if ( moda_menorIgual  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_menorIgual++
            }else{
                N1++
            }
        }

        if ( line.indexOf(igual) != -1 ){
            if ( moda_igual  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_igual++
            }else{
                N1++
            }
        }

        if ( line.indexOf(diferente) != -1 ){
            if ( moda_diferente  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_diferente++
            }else{
                N1++
            }
        }

        if ( line.indexOf(and) != -1 ){
            if ( moda_and  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_and++
            }else{
                N1++
            }
        }

        if ( line.indexOf(or) != -1 ){
            if ( moda_or  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_or++
            }else{
                N1++
            }
        }

        if ( line.indexOf(mientras) != -1 ){
            if ( moda_mientras  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_mientras++
            }else{
                N1++
            }
        }

        if ( line.indexOf(repite) != -1 ){
            if ( moda_repite  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_repite++
            }else{
                N1++
            }
        }

        if ( line.indexOf(si) != -1 ){
            if ( moda_si  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_si++
            }else{
                N1++
            }
        }

        if ( line.indexOf(para) != -1 ){
            if ( moda_para  == 0 ){  //INCREMENTA CONTADOR
                N1++
                n1++
                moda_para++
            }else{
                N1++
            }
        }

        if ( line.indexOf(mil) != -1 ){
            if ( moda_mil  == 0 ){  //INCREMENTA CONTADOR
                N2++;
                n2++
                moda_mil++
            }else{
                N2++;
            }
        }

        if ( line.indexOf(ventiuno) != -1 ){
            if ( moda_ventiuno  == 0 ){  //INCREMENTA CONTADOR
                N2++;
                n2++
                moda_ventiuno++
            }else{
                N2++;
            }
        }

        if ( line.indexOf(cero) != -1 ){
            if ( moda_cero  == 0 ){  //INCREMENTA CONTADOR
                N2++;
                n2++
                moda_cero++
            }else{
                N2++;
            }
        }

        if ( line.indexOf(uno) != -1 ){
            if ( moda_uno  == 0 ){  //INCREMENTA CONTADOR
                N2++;
                n2++
                moda_uno++
            }else{
                N2++;
            }
        }

        if ( line.indexOf(dos) != -1 ){
            if ( moda_dos  == 0 ){  //INCREMENTA CONTADOR
                N2++;
                n2++
                moda_dos++
            }else{
                N2++;
            }
        }

    }


}
