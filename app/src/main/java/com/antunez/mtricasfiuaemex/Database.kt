package com.antunez.mtricasfiuaemex

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_database.*

class Database : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_database)

        leeMetricas()

        //Acción Boton Regresar
        btn_Regresar.setOnClickListener {
            onBackPressed()
        }
    }

    val context = this
    var db = DataBaseHandler(context)

    private fun leeMetricas(){
        var data = db.readData()  //SE INSTANCÍA PARA LEER LOS DATOS DE LA BASE
        println("")

        val stringBuilder = StringBuilder()
        var line: String? = ""

        for ( i in 0..(data.size -1)  ){
            stringBuilder.append( data.get(i).arch.toString() + "  " + data.get(i).met_n1.toString() + "   " + data.get(i).met_N1.toString()
                    + "   " + data.get(i).met_n2.toString() + "   " + data.get(i).met_N2.toString() + "  " + data.get(i).met_N.toString()
                    + "   " + data.get(i).met_n.toString() + "  " + data.get(i).met_V.toString() + "  " + data.get(i).met_D.toString()
                    + " " + data.get(i).met_L.toString() + "  " + data.get(i).met_E.toString() + "  " + data.get(i).met_T.toString()
                    + "   " + data.get(i).met_B.toString()
                    + "\n\n")

            println("* * * * * * * * * * * * * * * ID: "+data.get(i).id.toString())
            println("* * * * * * * * * * * * * NOMBRE: "+data.get(i).arch.toString())
        }
        txt_ContenidoMetricas.setText(stringBuilder.toString())  //SETEAMOS TODA LA BASE
    }
}
