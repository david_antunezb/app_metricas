package com.antunez.mtricasfiuaemex

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_resultados.*

class Resultados : AppCompatActivity() {

    //DEFINICIÓN DE ATRIBUTOS
    private var nom_Arch = ""
    private var n1 = 0.0  //Numero de Operadores
    private var N1 = 0    //Total de Operadores
    private var n2 = 0.0  //Numero de Operandos
    private var N2 = 0    //Total de Operandos
    private var N = 0     //Longitud del Programa
    private var n = 0.0   //Vocabulario
    private var V = 0.0   //Volumen
    private var D = 0.0   //Dificultad
    private var L = 0.0   //Nivel del Programa
    private var E = 0.0   //Esfuerzo
    private var T = 0.0   //Tiempo (seg)
    private var B = 0.0   //Número de Bugs


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resultados)

        //Mostramos Mensaje de Satisfacción para El Guardado en la Base de Datos
        showAlert("Los Resultados Se Han Guardado En La Base De Datos")

        //Recuperamos los valores
        val objetoIntent: Intent = intent
        nom_Arch = objetoIntent.getStringExtra("nom_Arch")
        n1= objetoIntent.getDoubleExtra("n1", n1)
        N1= objetoIntent.getIntExtra("N1", N1)
        n2= objetoIntent.getDoubleExtra("n2", n2)
        N2= objetoIntent.getIntExtra("N2", N2)

        N= objetoIntent.getIntExtra("N", N)
        n= objetoIntent.getDoubleExtra("n", n)
        V= objetoIntent.getDoubleExtra("V", V)
        D= objetoIntent.getDoubleExtra("D", D)
        L= objetoIntent.getDoubleExtra("L", L)
        E= objetoIntent.getDoubleExtra("E", E)
        T= objetoIntent.getDoubleExtra("T", T)
        B= objetoIntent.getDoubleExtra("B", B)

        //Seteamos los TextView En la vista
        val_n1.text = ""+n1
        val_N1.text = ""+N1
        val_n2.text = ""+n2
        val_N2.text = ""+N2
        val_N.text = ""+N
        val_n.text = ""+n
        val_V.text = ""+V
        val_D.text = ""+D
        val_L.text = ""+L
        val_E.text = ""+E
        val_T.text ="$T seg"
        val_B.text = ""+B


        //Guardamos en la Base de Datos
        guardarMetricas()

        leeMetricas()


        //Acción Botón Regresar
        btn_Regresar.setOnClickListener {
            onBackPressed()
            //Regresará al Menú Principal
            //val intent: Intent = Intent(this, Home::class.java)
            //startActivity(intent)
        }

    }

    //Función que Escribe en la Base de Datos
    val context = this
    var db = DataBaseHandler(context)
    private fun guardarMetricas(){
        var database = Database_Metricas(nom_Arch, n1.toString(), N1.toString(), n2.toString(), N2.toString(), N.toString(), n.toString(), V.toString(), D.toString(), L.toString(), E.toString(), T.toString(), B.toString())
        var db = DataBaseHandler(context)

        db.insertData(database)
    }

    private fun leeMetricas(){
        var data = db.readData()

        println("")
        for ( i in 0..(data.size -1)  ){
            println("* * * * * * * * * * * * * * * ID: "+data.get(i).id.toString())
            println("* * * * * * * * * * * * * NOMBRE: "+data.get(i).arch.toString())

        }
    }

    //Función para mostrar Ventana de Alert
    private fun showAlert(message: String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Análisis Completado")
        builder.setMessage(message)

        val dialog = builder.create()
        dialog.show()
    }





}

