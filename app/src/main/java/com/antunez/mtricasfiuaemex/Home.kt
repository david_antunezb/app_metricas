package com.antunez.mtricasfiuaemex

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_main.*

class Home : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        //Acción del Botón Regresar
        btn_Regresar.setOnClickListener {
            onBackPressed()  //Regresa a Vista Anterior(MainActivity)
        }

        //Acción del Botón Calcular Métricas
        btn_Metricas.setOnClickListener {
            val intent:Intent = Intent(this, Metricas::class.java)
            startActivity(intent)
        }

        //Acción del Botón Concultar Base de Datos
        btn_Base.setOnClickListener {
            val intent:Intent = Intent(this, Database::class.java)
            startActivity(intent)
        }

    }
}
