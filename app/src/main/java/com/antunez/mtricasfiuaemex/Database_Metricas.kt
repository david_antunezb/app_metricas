package com.antunez.mtricasfiuaemex

class Database_Metricas{

    //ATRIBUTOS PARA LA TABLA
    var id : Int = 0
    var arch : String = ""
    var met_n1 : String = ""
    var met_N1 : String = ""
    var met_n2 : String = ""
    var met_N2 : String = ""
    var met_N : String = ""
    var met_n : String = ""
    var met_V : String = ""
    var met_D : String = ""
    var met_L : String = ""
    var met_E : String = ""
    var met_T : String = ""
    var met_B : String = ""

    constructor(arch: String, met_n1: String, met_N1: String, met_n2: String, met_N2: String, met_N: String, met_n: String, met_V: String, met_D: String, met_L: String, met_E: String, met_T: String, met_B: String){
        this.arch = arch
        this.met_n1 = met_n1
        this.met_N1 = met_N1
        this.met_n2 = met_n2
        this.met_N2 = met_N2
        this.met_N = met_N
        this.met_n = met_n
        this.met_V = met_V
        this.met_D = met_D
        this.met_L = met_L
        this.met_E = met_E
        this.met_T = met_T
        this.met_B = met_B
    }

    constructor(){

    }


}