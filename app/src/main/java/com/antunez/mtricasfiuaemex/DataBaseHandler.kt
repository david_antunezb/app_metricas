package com.antunez.mtricasfiuaemex

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.text.DecimalFormat


val DATABASE_NAME = "metricas"   //NOMBRE DE LA BASE DE DATOS
val TABLE_NAME = "resultados"     //NOMBRE DE LA TABLA
val COL_ID = "id"                 //NOMBRE DE LOS CAMPOS DE LA TABLA
val COL_ARCHIVO = "name_Archivo"
val COL_NOPERADORES = "met_N_OPERADORES_n1"
val COL_TOTALOPERADORES = "met_TOTAL_OPERADORES_N1"
val COL_NOPERANDOS = "met_N_OPERANDOS_n2"
val COL_TOTALOPERANDOS = "met_TOTAL_OPERANDOS_N2"
val COL_LONGITUD = "met_LONGITUD_N"
val COL_VOCABULARIO = "met_VOCABULARIO_n"
val COL_VOLUMEN = "met_V"
val COL_DIFICULTAD = "met_D"
val COL_NIVEL = "met_L"
val COL_ESFUERZO = "met_E"
val COL_TIEMPO = "met_T"
val COL_BUGS = "met_B"

class DataBaseHandler(context: Context):SQLiteOpenHelper(context, DATABASE_NAME, null,1){
    override fun onCreate(db: SQLiteDatabase?) {
        val createTable = "CREATE TABLE " +  TABLE_NAME + " ("+
                COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COL_ARCHIVO + " VARCHAR(20)," +
                COL_NOPERADORES + " VARCHAR(20)," +
                COL_TOTALOPERADORES + " VARCHAR(20)," +
                COL_NOPERANDOS + " VARCHAR(20)," +
                COL_TOTALOPERANDOS + " VARCHAR(20)," +
                COL_LONGITUD + " VARCHAR(20)," +
                COL_VOCABULARIO + " VARCHAR(20)," +
                COL_VOLUMEN + " VARCHAR(20)," +
                COL_DIFICULTAD + " VARCHAR(20)," +
                COL_NIVEL + " VARCHAR(20)," +
                COL_ESFUERZO + " VARCHAR(20)," +
                COL_TIEMPO + " VARCHAR(20)," +
                COL_BUGS + " VARCHAR(20) )"

        db?.execSQL(createTable)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun insertData(metricas: Database_Metricas ){
        val db = this.writableDatabase
        var cv = ContentValues()
        cv.put(COL_ARCHIVO, metricas.arch)
        cv.put(COL_NOPERADORES, metricas.met_n1)
        cv.put(COL_TOTALOPERADORES, metricas.met_N1)
        cv.put(COL_NOPERANDOS, metricas.met_n2)
        cv.put(COL_TOTALOPERANDOS, metricas.met_N2)
        cv.put(COL_LONGITUD, metricas.met_N)
        cv.put(COL_VOCABULARIO, metricas.met_n)
        cv.put(COL_VOLUMEN, metricas.met_V)
        cv.put(COL_DIFICULTAD, metricas.met_D)
        cv.put(COL_NIVEL, metricas.met_L)
        cv.put(COL_ESFUERZO, metricas.met_E)
        cv.put(COL_TIEMPO, metricas.met_T)
        cv.put(COL_BUGS, metricas.met_B)

        var result = db.insert(TABLE_NAME, null, cv)

        //val context = this

        if ( result == -1.toLong() ){
            println("* * * * * * * * * * La Conexión a la Base de Datos Falló")
        }else{
            println("* * * * * * * * * * Los Datos se han guardado Correctamente")
        }
    }

    fun readData(): MutableList<Database_Metricas>{
        var list : MutableList<Database_Metricas> = ArrayList()

        val db = this.readableDatabase
        val query = "SELECT * FROM " + TABLE_NAME

        val result = db.rawQuery(query, null)

        val formato=DecimalFormat("#.00")

        //val doble: Double=cadena.toDouble()

        //val cadena: String = ""
        //val valor= 0.00
        if ( result.moveToFirst() ){
            do{
                var database =  Database_Metricas()
                database.id =  result.getString(result.getColumnIndex(COL_ID)).toInt()

                database.arch = result.getString(result.getColumnIndex(COL_ARCHIVO))


                val cadena = result.getString(result.getColumnIndex(COL_NOPERADORES))  //DATO DE LA BASE
                val valor: Double=cadena.toDouble()  //LA CADENA LA PASAMOS A DOUBLE
                val dato = formato.format(valor)   //LE DAMOS FORMATO DE 2 DECIMALES
                database.met_n1 = dato.toString()   //ENVIAMOS CADENA CON FORMATO


                val cadena2 = result.getString(result.getColumnIndex(COL_TOTALOPERADORES))  //DATO DE LA BASE
                val valor2: Double=cadena2.toDouble()  //LA CADENA LA PASAMOS A DOUBLE
                val dato2 = formato.format(valor2)   //LE DAMOS FORMATO DE 2 DECIMALES
                database.met_N1 = dato2.toString()

                val cadena3 = result.getString(result.getColumnIndex(COL_NOPERANDOS))  //DATO DE LA BASE
                val valor3: Double=cadena3.toDouble()  //LA CADENA LA PASAMOS A DOUBLE
                val dato3 = formato.format(valor3)   //LE DAMOS FORMATO DE 2 DECIMALES
                database.met_n2 = dato3.toString()

                val cadena4 = result.getString(result.getColumnIndex(COL_TOTALOPERANDOS))  //DATO DE LA BASE
                val valor4: Double=cadena4.toDouble()  //LA CADENA LA PASAMOS A DOUBLE
                val dato4 = formato.format(valor4)   //LE DAMOS FORMATO DE 2 DECIMALES
                database.met_N2 = dato4.toString()

                val cadena5 = result.getString(result.getColumnIndex(COL_LONGITUD))  //DATO DE LA BASE
                val valor5: Double=cadena5.toDouble()  //LA CADENA LA PASAMOS A DOUBLE
                val dato5 = formato.format(valor5)   //LE DAMOS FORMATO DE 2 DECIMALES
                database.met_N = dato5.toString()

                val cadena6 = result.getString(result.getColumnIndex(COL_VOCABULARIO))  //DATO DE LA BASE
                val valor6: Double=cadena6.toDouble()  //LA CADENA LA PASAMOS A DOUBLE
                val dato6 = formato.format(valor6)   //LE DAMOS FORMATO DE 2 DECIMALES
                database.met_n = dato6.toString()


                val cadena7 = result.getString(result.getColumnIndex(COL_VOLUMEN))  //DATO DE LA BASE
                val valor7: Double=cadena7.toDouble()  //LA CADENA LA PASAMOS A DOUBLE
                val dato7 = formato.format(valor7)   //LE DAMOS FORMATO DE 2 DECIMALES
                database.met_V = dato7.toString()

                val cadena8 = result.getString(result.getColumnIndex(COL_DIFICULTAD))  //DATO DE LA BASE
                val valor8: Double=cadena8.toDouble()  //LA CADENA LA PASAMOS A DOUBLE
                val dato8 = formato.format(valor8)   //LE DAMOS FORMATO DE 2 DECIMALES
                database.met_D = dato8.toString()

                val cadena9 = result.getString(result.getColumnIndex(COL_NIVEL))  //DATO DE LA BASE
                val valor9: Double=cadena9.toDouble()  //LA CADENA LA PASAMOS A DOUBLE
                val dato9 = formato.format(valor9)   //LE DAMOS FORMATO DE 2 DECIMALES
                database.met_L = dato9.toString()

                val cadena10 = result.getString(result.getColumnIndex(COL_ESFUERZO))  //DATO DE LA BASE
                val valor10: Double=cadena10.toDouble()  //LA CADENA LA PASAMOS A DOUBLE
                val dato10 = formato.format(valor10)   //LE DAMOS FORMATO DE 2 DECIMALES
                database.met_E = dato10.toString()

                val cadena11 = result.getString(result.getColumnIndex(COL_TIEMPO))  //DATO DE LA BASE
                val valor11: Double=cadena11.toDouble()  //LA CADENA LA PASAMOS A DOUBLE
                val dato11 = formato.format(valor11)   //LE DAMOS FORMATO DE 2 DECIMALES
                database.met_T = dato11.toString()

                val cadena12 = result.getString(result.getColumnIndex(COL_BUGS))  //DATO DE LA BASE
                val valor12: Double=cadena12.toDouble()  //LA CADENA LA PASAMOS A DOUBLE
                val dato12 = formato.format(valor12)   //LE DAMOS FORMATO DE 2 DECIMALES
                database.met_B = dato12.toString()


                //database.met_n1 =  result.getString(result.getColumnIndex(COL_NOPERADORES))
                //database.met_N1 =  result.getString(result.getColumnIndex(COL_TOTALOPERADORES))
                //database.met_n2 =  result.getString(result.getColumnIndex(COL_NOPERANDOS))
                //database.met_N2 =  result.getString(result.getColumnIndex(COL_TOTALOPERANDOS))
                //database.met_N =  result.getString(result.getColumnIndex(COL_LONGITUD))
                //database.met_n =  result.getString(result.getColumnIndex(COL_VOCABULARIO))
                //database.met_V =  result.getString(result.getColumnIndex(COL_VOLUMEN))
                //database.met_D =  result.getString(result.getColumnIndex(COL_DIFICULTAD))
                //database.met_L =  result.getString(result.getColumnIndex(COL_NIVEL))
                //database.met_E =  result.getString(result.getColumnIndex(COL_ESFUERZO))
                //database.met_T =  result.getString(result.getColumnIndex(COL_TIEMPO))
                //database.met_B =  result.getString(result.getColumnIndex(COL_BUGS))

                list.add(database)
            }while (result.moveToNext())
        }

        result.close()
        db.close()

        return list
    }

    //Función para mostrar Ventana de Alert
    /*private fun showAlert(message: String){
        val context = this
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Base de Datos")
        builder.setMessage(message)

        val dialog = builder.create()
        dialog.show()
    }*/

}
